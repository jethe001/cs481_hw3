import 'package:flutter/material.dart';

void main() => runApp(MaterialApp(
  home: Home(),
));

class Home extends StatefulWidget {

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {

  int hearts = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[900],
      appBar: AppBar(
        title: Text('Minions',
            style: TextStyle(
              color: Colors.black,
            )),
        backgroundColor: Colors.yellowAccent,
        elevation: 0,
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          setState(() {
            hearts++;
          });
        },
        child: Icon(Icons.favorite),
        backgroundColor: Colors.red,
      ),

      body:
      new ListView(
      children:<Widget>[

      Padding(
        padding: EdgeInsets.fromLTRB(10, 20, 10, 0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Center(
              child: Image.asset('images/animate.gif'),

            ),
            Divider(
              height: 25,
              color: Colors.white,
            ),
            Text(
              'Name',
              style: TextStyle(
                  color: Colors.white,
                  fontStyle: FontStyle.italic,
                  letterSpacing: 2
              ),
            ),
            SizedBox(height: 5),
            Text(
              'Robert Bob',
              style: TextStyle(
                  color: Colors.amberAccent[200],
                  letterSpacing: 2,
                  fontSize: 28,
                  fontWeight: FontWeight.bold
              ),
            ),
            SizedBox(height: 10),

            Text(
              'Hearts Obtained',
              style: TextStyle(
                  color: Colors.white,
                  fontStyle: FontStyle.italic,
                  letterSpacing: 2
              ),
            ),
            SizedBox(height: 5),
            Text(
              '$hearts',
              style: TextStyle(
                  color: Colors.amberAccent[200],
                  letterSpacing: 2,
                  fontSize: 28,
                  fontWeight: FontWeight.bold
              ),
            ),
            Text(
              'About',
              style: TextStyle(
                  color: Colors.white,
                  fontStyle: FontStyle.italic,
                  letterSpacing: 2
              ),
            ),
            SizedBox(height: 10),
            Text(
              'Bob is a minion who is more childish and immature than most of the other minions. \nHe is described as a "Little Brother" who finds love in anything and everything.',
              style: TextStyle(
                  color: Colors.yellow,
                  letterSpacing: 2
              ),
            ),

            SizedBox(height: 10),

            Row(
              children: <Widget>[
                Icon(
                  Icons.email,
                  color: Colors.yellow,
                ),
                SizedBox(
                  width: 10,
                ),
                Text(
                  'bob@minions.com',
                  style: TextStyle(
                      color: Colors.grey[400],
                      fontSize: 18,
                      letterSpacing: 1
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    ],
      ),
    );
  }
}